import React, { Component } from 'react';
import './App.css';
import Rx from 'rxjs/Rx';
import { createStore } from 'redux';
import timerApp from './reducers';
import { updateCount } from './actions'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {count: 0};
    this.store = createStore(timerApp);

    //subscribe to the redux store and fetch new count when store is updated
    this.store.subscribe(() => {
      this.setState({
        count: this.store.getState().count
      });
    });
  }

  render() {
    return (
      <div className="App">
        <Timer count={this.state.count} />
      </div>
    );
  }

  //create the rxjs timer and subscribe to it
  componentDidMount() {
    var timer = Rx.Observable.interval(2000);
    timer.subscribe(count => this.updateReduxCount(count));

  }

  //update the redux store with new count
  updateReduxCount(count) {
    this.store.dispatch(updateCount(count));
  }


}

class Timer extends Component {

  render() {
    return (
      <div className="Timer">
        <h1>Timer Application</h1>
        <div>
          {this.props.count * 2} <span className="Bold"> Seconds </span>
        </div>
        <div>
          <span className="Bold"> Times ran: </span> {this.props.count}
        </div>
      </div>
    );
  }


}

export default App;
