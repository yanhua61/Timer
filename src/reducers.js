import { UPDATE_COUNT } from './actions'
const initialState = {
  count: 0
}

export default function timerApp(state = initialState, action) {
  switch (action.type) {
    case UPDATE_COUNT:
      return Object.assign({}, state, { count: action.count } );
    default:
      return state;

  }
}
